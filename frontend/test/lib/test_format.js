
import assert from "assert";
import { formatAttributeValue } from "../../lib/format";

describe("formatAttributeValue()", function() {
    const stringType = { unit: "", data_type: "string" };
    const numericType = { unit: "F", data_type: "number" };

    it("should return empty string when value is blank", function () {
        assert.equal(formatAttributeValue(null, stringType), "");
        assert.equal(formatAttributeValue(undefined, stringType), "");
        assert.equal(formatAttributeValue("", stringType), "");
        assert.equal(formatAttributeValue(null, numericType), "");
        assert.equal(formatAttributeValue(undefined, numericType), "");
        assert.equal(formatAttributeValue("", numericType), "");
    });

    it("should return same value when data type is string", function () {
        assert.equal(formatAttributeValue("hello", stringType), "hello");

        // Possible a numeric value should be converted to a string type...
        assert.strictEqual(formatAttributeValue(15, stringType), 15);
    });

    it("should return numeric value with suitable metric prefix", function () {
        const tests = [
            [ 0.000000000000001, "0.001 pF" ],
            [ 0.00000000000001, "0.01 pF" ],
            [ 0.0000000000001, "0.1 pF" ],
            [ 0.000000000001, "1 pF" ],
            [ 0.00000000001, "10 pF" ],
            [ 0.0000000001, "100 pF" ],
            [ 0.000000001, "1 nF" ],
            [ 0.00000001, "10 nF" ],
            [ 0.0000001, "100 nF" ],
            [ 0.000001, "1 uF" ],
            [ 0.00001, "10 uF" ],
            [ 0.0001, "100 uF" ],
            [ 0.001, "1 mF" ],
            [ 0.01, "10 mF" ],
            [ 0.1, "0.1 F" ],
            [ 0, "0 F" ],
            [ 1, "1 F" ],
            [ 10, "10 F" ],
            [ 100, "100 F" ],
            [ 1000, "1 kF" ],
            [ 10000, "10 kF" ],
            [ 100000, "100 kF" ],
            [ 1000000, "1 MF" ],
            [ 10000000, "10 MF" ],
            [ 100000000, "100 MF" ],
            [ 1000000000, "1 GF" ],
            [ 10000000000, "10 GF" ]
        ];

        tests.forEach(test => assert.equal(formatAttributeValue(test[0], numericType), test[1]));
    });
});
