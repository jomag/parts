
// See MarkG's answer here:
// https://stackoverflow.com/questions/11832914
function roundToFive(num) {
    //return +(Math.round(num + "100000") + "0.00001");
    return Math.round(num * 1000) / 1000;
}

export function formatNumericAttributeValue(value, attributeType) {
    let metric = "";

    if (value >= 1000000000) {
        metric = "G";
        value = value / 1000000000;
    } else if (value >= 1000000) {
        metric = "M";
        value = value / 1000000;
    } else if (value >= 1000) {
        metric = "k";
        value = value / 1000;
    } else if (value >= 0.1) {
        // Do nothing
    } else if (value >= 0.001) {
        metric = "m";
        value = value * 1000;
    } else if (value >= 0.000001) {
        metric = 'u';
        value = value * 1000000;
    } else if (value >= 0.000000001) {
        metric = 'n';
        value = value * 1000000000;
    } else if (value >= 0.000000000000001) {
        metric = 'p';
        value = value * 1000000000000;
    }

    if (attributeType.unit) {
        metric = metric + attributeType.unit;
    }

    return roundToFive(value) + " " + metric;
}

export function formatAttributeValue(value, attributeType) {
    if (value === "" || value === null || value === undefined)
        return "";

    if (attributeType.data_type === "number")
        return formatNumericAttributeValue(value, attributeType);
    else
        return value;
}
