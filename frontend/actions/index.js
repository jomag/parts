
import axios from "axios";

export const FETCH_MANUFACTURER_INDEX = "FETCH_MANUFACTURER_INDEX";
export const FETCH_MANUFACTURER = "FETCH_MANUFACTURER";
export const FETCH_DISTRIBUTOR_INDEX = "FETCH_DISTRIBUTOR_INDEX";
export const FETCH_COMPONENT_INDEX = "FETCH_COMPONENT_INDEX";
export const FETCH_COMPONENT = "FETCH_COMPONENT";
export const FETCH_CATEGORY_INDEX = "FETCH_CATEGORY_INDEX";
export const FETCH_CATEGORY = "FETCH_CATEGORY";
export const FETCH_BOM_INDEX = "FETCH_BOM_INDEX";
export const FETCH_BOM = "FETCH_BOM";

export const fetchManufacturerIndex = () => {
    return {
        type: FETCH_MANUFACTURER_INDEX,
        payload: axios({ method: "GET", url: "/api/1.0/manufacturers"})
    }
}

export const fetchManufacturer = (id) => {
    return {
        type: FETCH_MANUFACTURER,
        payload: axios({ method: "GET", url: "/api/1.0/manufacturers/" + id})
    }
}

export const fetchDistributorIndex = () => {
    return {
        type: FETCH_DISTRIBUTOR_INDEX,
        payload: axios({ method: "GET", url: "/api/1.0/distributors"})
    }
}

export const fetchComponentIndex = () => {
    return {
        type: FETCH_COMPONENT_INDEX,
        payload: axios({ method: "GET", url: "/api/1.0/components"})
    }
}

export const fetchComponent = (id) => {
    return {
        type: FETCH_COMPONENT,
        payload: axios({ method: "GET", url: "/api/1.0/components/" + id})
    }
}

export const fetchCategoryIndex = () => {
    return {
        type: FETCH_CATEGORY_INDEX,
        payload: axios({ method: "GET", url: "/api/1.0/categories"})
    }
}

export const fetchCategory = (id) => {
    return {
        type: FETCH_CATEGORY,
        payload: axios({ method: "GET", url: "/api/1.0/categories/" + id})
    }
}

export const fetchBOMIndex = () => {
    return {
        type: FETCH_BOM_INDEX,
        payload: axios({ method: "GET", url: "/api/1.0/boms"})
    }
}

export const fetchBom = (id) => {
    return {
        type: FETCH_BOM,
        payload: axios({ method: "GET", url: "/api/1.0/boms/" + id})
    }
}
