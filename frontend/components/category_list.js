
import React, { Component } from "react";
import { Link } from "react-router-dom";
import { mapValues, sortBy } from "lodash";

const renderRow = (category) => {
    let link = "/categories/" + category.id;

    return (
      <tr key={category.id}>
        <td><Link to={link}>{category.id}</Link></td>
        <td><Link to={link}>{category.name}</Link></td>
        <td>{category.components.length}</td>
        <td>{category.notes}</td>
      </tr>
    );
}

const CategoryList = ({ categories }) => {
    let list = mapValues(categories);
    list = sortBy(list, ["name", "id"]);

    return (
        <table className="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Components</th>
                    <th>Notes</th>
                </tr>
            </thead>
            <tbody>
                { list.map(renderRow) }
            </tbody>
        </table>
    );
}

export default CategoryList;
