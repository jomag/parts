
import React, { Component } from "react";
import { mapValues, sortBy } from "lodash";

const renderRow = (manufacturer) => (
    <tr key={manufacturer.id}>
        <td>{manufacturer.id}</td>
        <td>{manufacturer.name}</td>
    </tr>
)

const ManufacturerList = ({ manufacturers }) => {
    let list = mapValues(manufacturers);
    list = sortBy(list, ["name", "id"])
    return (
        <table className="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                </tr>
            </thead>
            <tbody>
                { list.map(renderRow) }
            </tbody>
        </table>
    );
}

export default ManufacturerList;
