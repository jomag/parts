
import React, { Component } from "react";
import { mapValues, sortBy } from "lodash";
import { Link } from "react-router-dom";

import { formatAttributeValue } from "../lib/format";

const renderRow = (component, manufacturers, attributeTypes) => {
    const manufacturer = manufacturers[component.manufacturer];
    let manufacturer_col = "";

    if (manufacturer) {
        manufacturer_col = <Link to={"/manufacturers/" + manufacturer.id}>{manufacturer.name}</Link>;
    }

    let link = "/components/" + component.id
    let id_col = <Link to={link}>{component.id}</Link>
    let part_id_col = <Link to={link}>{component.part_id}</Link>
    let attributes;
    let packageName;

    if (attributeTypes) {
        attributes = attributeTypes.map(attr => {
            if (component.attributes.hasOwnProperty(attr.id)) {
                const value = formatAttributeValue(component.attributes[attr.id].value, attr);
                return (<td key={attr.id}>{value}</td>);
            } else {
                return (<td key={attr.id}></td>);
            }
        });
    }
    const actions = <a href={"/admin/main/component/" + component.id + "/change"}>edit</a>;

    if (component.package) {
        packageName = component.package.name;
    }

    return (
        <tr key={component.id}>
            <td>{id_col}</td>
            <td>{part_id_col}</td>
            <td>{manufacturer_col}</td>
            <td>{component.manufacturer_pn}</td>
            <td>{packageName}</td>
            {attributes}
            <td>{component.in_stock}</td>
            <td>{actions}</td>
        </tr>
    );
}

const ComponentList = ({ components, manufacturers, category }) => {
    let list = mapValues(components);
    list = sortBy(list, ["part_id", "id"]);

    let attributeTypes;
    let attributeTypeHeaders;

    if (category) {
        attributeTypes = category.attribute_types;
        attributeTypeHeaders = attributeTypes.map(attr => <th key={attr.id}>{attr.name}</th>);
    }

    return (
        <table className="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Part ID</th>
                    <th>Manufacturer</th>
                    <th>Manufacturer P/N</th>
                    <th>Package</th>
                    { attributeTypeHeaders }
                    <th>In stock</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                { list.map(component => renderRow(component, manufacturers, attributeTypes)) }
            </tbody>
        </table>
    );
}

export default ComponentList;
