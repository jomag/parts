
import React, { Component } from "react";
import { Link } from "react-router-dom";
import { mapValues, sortBy } from "lodash";

const renderRow = (bom) => {
    const link = "/boms/" + bom.id;

    return (
        <tr key={bom.id}>
            <td><Link to={link}>{bom.id}</Link></td>
            <td><Link to={link}>{bom.name}</Link></td>
            <td>{bom.notes}</td>
        </tr>
    );
}

const BOMList = ({ boms }) => {
    let list = mapValues(boms);
    list = sortBy(list, ["part_id", "id"]);
    return (
        <table className="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Notes</th>
                </tr>
            </thead>
            <tbody>
                { list.map(renderRow) }
            </tbody>
        </table>
    );
}

export default BOMList;
