
import React, { Component } from "react";

const renderRow = (distributor) => (
    <tr key={distributor.id}>
        <td>{distributor.id}</td>
        <td>{distributor.name}</td>
    </tr>
)

const DistributorList = ({ distributors }) => (
    <table className="table">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
            </tr>
        </thead>
        <tbody>
            { distributors.map(renderRow) }
        </tbody>
    </table>
)

export default DistributorList;
