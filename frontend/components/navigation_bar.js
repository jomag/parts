
import React, { Component } from "react";
import { Navbar, Nav , NavItem } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";

function navItems() {
    const links = [
        ["manufacturers", "/manufacturers"],
        ["Distributors", "/distributors"],
        ["Components", "/components"],
        ["Categories", "/categories"],
        ["BOM", "/boms"]
    ];

    return links.map(x => (
        <LinkContainer key={x[1]} to={x[1]}>
            <NavItem>{x[0]}</NavItem>
        </LinkContainer>
    ))
}

const NavigationBar = ({ title }) => (
    <Navbar inverse collapseOnSelect fluid staticTop>
        <Navbar.Header>
            <Navbar.Brand>
                <a className="navbar-brand" href="#">{ title }</a>
            </Navbar.Brand>
            <Navbar.Toggle/>
        </Navbar.Header>

        <Nav>
            {navItems()}
        </Nav>
    </Navbar>
)

export default NavigationBar;
