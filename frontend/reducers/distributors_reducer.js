
import { FETCH_DISTRIBUTOR_INDEX, FETCH_COMPONENT } from "../actions";

const distributorsReducer = (state=[], action) => {
    switch (action.type) {
    case FETCH_DISTRIBUTOR_INDEX:
        return action.payload.data;

    case FETCH_COMPONENT:
        {
            const data = action.payload.data;
            const distributors = {}
            for (let i = 0; i < data.distributors.length; i++) {
                const distributor = data.distributors[i].distributor;
                if (data.distributors[i].distributor) {
                    distributors[distributor.id] = distributor;
                }
            }
            return {...state, ...distributors};
        }

    default:
        return state;
    }
};

export default distributorsReducer;
