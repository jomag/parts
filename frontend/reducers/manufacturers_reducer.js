
import { mapKeys} from "lodash";
import { FETCH_MANUFACTURER_INDEX, FETCH_MANUFACTURER } from "../actions";

const manufacturersReducer = (state={}, action) => {
    switch (action.type) {
    case FETCH_MANUFACTURER_INDEX:
        const manufacturers = mapKeys(action.payload.data, "id");
        return {...state, ...manufacturers};
    case FETCH_MANUFACTURER:
        const data = action.payload.data;
        const manufacturer = {[data.id]: data};
        return {...state, ...manufacturer};
    default:
        return state;
    }
};

export default manufacturersReducer;
