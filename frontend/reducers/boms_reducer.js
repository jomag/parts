
import { mapKeys} from "lodash";
import { FETCH_BOM_INDEX, FETCH_BOM } from "../actions";

const bomReducer = (state={}, action) => {
    switch (action.type) {
    case FETCH_BOM_INDEX:
        {
            const boms = mapKeys(action.payload.data, "id");
            return {...state, ...boms};
        }
    case FETCH_BOM:
        {
            const data = action.payload.data;
            const bom = {[data.id]: data}
            return {...state, ...bom};
        }
    default:
        return state;
    }
};

export default bomReducer;
