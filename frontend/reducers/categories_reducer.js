
import { mapKeys } from "lodash";

import { FETCH_CATEGORY, FETCH_CATEGORY_INDEX } from "../actions";

const processCategory = (payload) => {
    const category = {...payload};
    category.components = Array.from(category.components, o => o.id);
    return category;
}

const categoriesReducer = (state=[], action) => {
    switch (action.type) {
    case FETCH_CATEGORY_INDEX:
        {
            const categoryList = action.payload.data.map(processCategory);
            const categories = mapKeys(categoryList, "id");
            return { ...state, ...categories };
        }
    case FETCH_CATEGORY:
        {
            const category = processCategory(action.payload.data);
            const categories = {[category.id]: category}
            return { ...state, ...categories };
        }
    default:
        return state;
    }
};

export default categoriesReducer;
