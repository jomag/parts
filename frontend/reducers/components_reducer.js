
import { mapKeys} from "lodash";
import { FETCH_COMPONENT_INDEX, FETCH_COMPONENT, FETCH_CATEGORY, FETCH_CATEGORY_INDEX } from "../actions";

const processComponent = (component) => {
    const c = {...component};
    c.attributes = mapKeys(c.attributes, "attribute_type");
    return c;
}

const componentsReducer = (state={}, action) => {
    switch (action.type) {
    case FETCH_COMPONENT_INDEX:
        {
            const data = action.payload.data;
            const components = mapKeys(data.map(processComponent), "id");
            return {...state, ...components};
        }

    case FETCH_COMPONENT:
        {
            const data = action.payload.data;
            const component = {[data.id]: processComponent(data)}
            let x = {...state, ...component};
            return x;
        }

    case FETCH_CATEGORY_INDEX:
        {
            const categories = action.payload.data;
            let newState = {...state}
            for (let i = 0; i < categories.length; i++) {
                let components = mapKeys(categories[i].components.map(processComponent), "id");
                newState = {...newState, ...components };
            }
            return newState;
        }
    case FETCH_CATEGORY:
        {
            const category = action.payload.data;
            const components = mapKeys(category.components.map(processComponent), "id", processComponent);
            return {...state, ...components };
        }
    default:
        return state;
    }
};

export default componentsReducer;
