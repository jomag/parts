
import { combineReducers } from "redux";

import manufacturers from "./manufacturers_reducer";
import distributors from "./distributors_reducer";
import components from "./components_reducer";
import categories from "./categories_reducer";
import boms from "./boms_reducer";

export default combineReducers({
    manufacturers,
    distributors,
    components,
    categories,
    boms
});
