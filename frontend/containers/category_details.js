
import React, { Component } from "react";
import ComponentList from "../components/component_list";
import { bindActionCreators }  from "redux";
import { connect }  from "react-redux";

import { fetchCategory } from "../actions";


class CategoryDetails extends Component {
    componentWillMount() {
        this.props.fetchCategory(this.props.id);
    }

    render() {
        const category = this.props.category;
        const components = this.props.components;
        const manufacturers = this.props.manufacturers;
        let title;

        if (category === undefined) {
            return <div><i>Loading</i></div>;
        }

        if (category.name) {
            title = category.name;
        } else {
            title = "New category";
        }

        return (
            <div>
                <h2>{title}</h2>
                <ComponentList components={components} manufacturers={manufacturers} category={category}/>
            </div>
        );
    }
}

function mapStateToProps(state, props) {
    const { match } = props;
    const id = match.params.id;
    const category = state.categories[id];
    const components = category.components.map(id => state.components[id])
    const manufacturers = state.manufacturers;
    return { id, category, components, manufacturers }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchCategory: bindActionCreators(fetchCategory, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(CategoryDetails);
