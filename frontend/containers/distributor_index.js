
import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import DistributorList from "../components/distributor_list";
import { fetchDistributorIndex } from "../actions";

class DistributorIndex extends Component {
    componentWillMount() {
        this.props.fetchDistributorIndex();
    }

    render() {
        return <DistributorList distributors={this.props.distributors}/>;
    }
}

function mapStateToProps(state) {
    return {
        distributors: state.distributors
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchDistributorIndex: bindActionCreators(fetchDistributorIndex, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(DistributorIndex);
