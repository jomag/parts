
import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import CategoryList from "../components/category_list";
import { fetchCategoryIndex } from "../actions";

class CategoryIndex extends Component {
    componentWillMount() {
        this.props.fetchCategoryIndex();
    }

    render() {
        return <CategoryList categories={this.props.categories}/>;
    }
}

function mapStateToProps(state) {
    return {
        categories: state.categories
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchCategoryIndex: bindActionCreators(fetchCategoryIndex, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(CategoryIndex);
