
import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import { fetchBom, fetchComponentIndex } from "../actions";

class BomDetails extends Component {
    componentWillMount() {
        this.props.fetchBom(this.props.id);
        this.props.fetchComponentIndex();
    }

    render() {
        const bom = this.props.bom;
        const components = this.props.components;

        let title;
        const renderRow = (item) => {
            const c = components[item.component] || {};
            const componentUrl = "/components/" + c.id;

            return (
                <tr key={item.id}>
                    <td>{item.part}</td>
                    <td><Link to={componentUrl}>{c.part_id}</Link></td>
                    <td>{item.notes}</td>
                </tr>
            );
        }


        if (bom === undefined) {
            return <div><i>Loading</i></div>;
        }

        if (bom.name) {
            title = "BOM: " + bom.name;
        } else {
            title = "New BOM";
        }

        console.log(bom);

        //let list = mapValues(bom.items);
        // list = sortBy(list ["part"]);
        const list = bom.items;

        return (
            <div>
                <h2>{title}</h2>

                <div>
                    {bom.notes || ""}
                </div>

                <table className="table">
                    <thead>
                        <tr>
                            <th>Part</th>
                            <th>Component</th>
                            <th>Notes</th>
                        </tr>
                    </thead>

                    <tbody>
                        { list.map(renderRow) }
                    </tbody>
                </table>
            </div>
        );
    }
}

function mapStateToProps(state, props) {
    const { match } = props;

    if (match.params.id) {
        const bom = state.boms[match.params.id];

        return {
            id: match.params.id,
            bom,
            components: state.components
        }
    } else {
        return {
            id: undefined,
            bom: undefined,
            components: state.components
        }
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchBom: bindActionCreators(fetchBom, dispatch),
        fetchComponentIndex: bindActionCreators(fetchComponentIndex, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(BomDetails);
