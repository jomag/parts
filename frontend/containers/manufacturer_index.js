
import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import ManufacturerList from "../components/manufacturer_list";
import { fetchManufacturerIndex } from "../actions";

class ManufacturerIndex extends Component {
    componentWillMount() {
        this.props.fetchManufacturerIndex();
    }

    render() {
        return <ManufacturerList manufacturers={this.props.manufacturers}/>;
    }
}

function mapStateToProps(state) {
    return {
        manufacturers: state.manufacturers
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchManufacturerIndex: bindActionCreators(fetchManufacturerIndex, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ManufacturerIndex);
