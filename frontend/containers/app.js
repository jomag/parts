
import React from "react";
import NavigationBar from "../components/navigation_bar";
import { BrowserRouter, Route } from "react-router-dom";

import ManufacturerIndex from "../containers/manufacturer_index";
import DistributorIndex from "../containers/distributor_index";
import CategoryIndex from "../containers/category_index";
import CategoryDetails from "../containers/category_details";
import ComponentIndex from "../containers/component_index";
import ComponentDetails from "../containers/component_details";
import BOMIndex from "../containers/bom_index";
import BomDetails from "../containers/bom_details";

const App = ({children}) => (
    <BrowserRouter>
        <div>
            <NavigationBar title="Parts"/>
            <Route path="/manufacturers" component={ManufacturerIndex}/>
            <Route path="/distributors" component={DistributorIndex}/>
            <Route exact path="/components" component={ComponentIndex}/>
            <Route path="/components/:id" component={ComponentDetails}/>
            <Route exact path="/categories" component={CategoryIndex}/>
            <Route path="/categories/:id" component={CategoryDetails}/>
            <Route exact path="/boms" component={BOMIndex}/>
            <Route path="/boms/:id" component={BomDetails}/>
        </div>
    </BrowserRouter>
)

export default App;
