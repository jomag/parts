
import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import BOMList from "../components/bom_list";
import { fetchBOMIndex } from "../actions";

class BOMIndex extends Component {
    componentWillMount() {
        this.props.fetchBOMIndex();
    }

    render() {
        return <BOMList boms={this.props.boms}/>;
    }
}

function mapStateToProps(state) {
    return {
        boms: state.boms
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchBOMIndex: bindActionCreators(fetchBOMIndex, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(BOMIndex);
