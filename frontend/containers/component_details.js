
import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { singular } from "pluralize";

import { fetchComponent, fetchCategory, fetchManufacturer } from "../actions";
import { formatAttributeValue } from "../lib/format";

const property = (prop, value, key) => (
    <tr key={key}>
        <th className="col-md-3">{prop}</th>
        <td className="col-md-9">{value}</td>
    </tr>
)


const ComponentCategoryAttributesTable = ({ component, category }) => {
    let attributeTypes = category.attribute_types;
    let rows = attributeTypes.map(attr => {
        let value;
        if (component.attributes.hasOwnProperty(attr.id)) {
            value = component.attributes[attr.id].value;
        }

        return property(attr.name, formatAttributeValue(value, attr), attr.id);
    })

    return (
        <table className="table">
            <tbody>
                {rows}
            </tbody>
        </table>
    );
}


const DistributorTable = ({ component }) => {
    let rows = component.distributors.map(distr => {
        console.debug(distr.id);
        return (
            <tr key={distr.id}>
                <td>{distr.distributor.name}</td>
                <td>{distr.ssk}</td>
                <td>{distr.price}</td>
            </tr>
        );
    });

    return (
        <table className="table">
            <thead>
                <th>Distributor</th>
                <th>Part Number</th>
                <th>Price</th>
            </thead>
            <tbody>
                {rows}
            </tbody>
        </table>
    )
}


class ComponentDetails extends Component {
    componentDidMount() {
        this.props.fetchComponent(this.props.id)
            .then(() => { this.props.fetchCategory(this.props.component.category) })
            .then(() => { this.props.fetchManufacturer(this.props.component.manufacturer) });
    }

    render() {
        const component = this.props.component;
        const manufacturer = this.props.manufacturer;
        const category = this.props.category;

        console.debug(component);

        let title;
        let manufacturerName;
        let categoryAttributes;
        let distributors;

        if (component === undefined) {
            return <div><i>Loading</i></div>;
        }

        if (component.part_id) {
            title = component.part_id;
        } else {
            title = "New component";
        }

        if (manufacturer) {
            manufacturerName = manufacturer.name;
        }

        if (category) {
            categoryAttributes = (
                <div>
                    <h3>{singular(category.name)} attributes</h3>
                    <ComponentCategoryAttributesTable component={component} category={category}/>
                </div>
            );
        }

        let editUrl = "/admin/main/component/" + component.id + "/change";
        let editLink =  <a className="btn btn-primary" role="button" href={editUrl}>Edit</a>;

        return (
            <div>
                <h2>{title} {editLink}</h2>

                <table className="table">
                    <tbody>
                        {property("Manufacturer", manufacturerName)}
                        {property("P/N", component.manufacturer_pn)}
                    </tbody>
                </table>

                {categoryAttributes}

                <h3>Distributors</h3>
                <DistributorTable component={component}/>
            </div>
        );
    }
}

function mapStateToProps(state, props) {
    const { match } = props;

    if (match.params.id) {
        const component = state.components[match.params.id];
        const manufacturer = component ? state.manufacturers[component.manufacturer] : null;
        const category = component ? state.categories[component.category] : null;
        return {
            id: match.params.id,
            component,
            category,
            manufacturer
        }
    } else {
        return {
            id: undefined,
            component: undefined,
            manufacturer: undefined,
            category: undefined
        }
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchComponent: bindActionCreators(fetchComponent, dispatch),
        fetchCategory: bindActionCreators(fetchCategory, dispatch),
        fetchManufacturer: bindActionCreators(fetchManufacturer, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ComponentDetails);
