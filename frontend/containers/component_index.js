
import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import ComponentList from "../components/component_list";
import { fetchComponentIndex } from "../actions";

class ComponentIndex extends Component {
    componentWillMount() {
        this.props.fetchComponentIndex();
    }

    render() {
        return <ComponentList components={this.props.components} manufacturers={this.props.manufacturers}/>;
    }
}

function mapStateToProps(state) {
    return {
        components: state.components,
        manufacturers: state.manufacturers
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchComponentIndex: bindActionCreators(fetchComponentIndex, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ComponentIndex);
