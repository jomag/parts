
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import reducers from "./reducers";
import ReduxPromise from "redux-promise";

import App from "./containers/app";

let store = createStore(reducers, applyMiddleware(ReduxPromise));

ReactDOM.render((
        <Provider store={store}>
            <App/>
        </Provider>
    ),
    document.getElementById("root")
);
