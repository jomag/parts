"""Production settings"""

import os

import dj_database_url

from parts.settings_shared import *

PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))
STATIC_ROOT = os.path.join(PROJECT_ROOT, "staticfiles")

STATICFILES_DIRS = [
    os.path.join(PROJECT_ROOT, "..", "static")
]

# Use whitenoise for serving static files
STATICFILES_STORAGE = "whitenoise.django.GzipManifestStaticFilesStorage"

ALLOWED_HOSTS = ["parts.herokuapp.com"]
DEBUG = False

SECRET_KEY = os.environ.get("DJANGO_SECRET_KEY")

DATABASES = {
    "default": dj_database_url.config(conn_max_age=600)
}

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,

    "handlers": {
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler"
        }
    },

    "loggers": {
        "django": {
            "handler": "console"
        }
    }
}

ADMINS = [("Jonatan Magnusson", "jonatan.magnusson@gmail.com")]
SERVER_EMAIL = "django@parts.herokuapp.com"

