
from rest_framework.serializers import ModelSerializer

from parts.main import models


class ManufacturerSerializer(ModelSerializer):
    class Meta:
        model = models.Manufacturer
        fields = ("id", "name", "notes")


class DistributorSerializer(ModelSerializer):
    class Meta:
        model = models.Distributor
        fields = ("id", "name", "notes", "lookup_url")


class PackageSerializer(ModelSerializer):
    class Meta:
        model = models.Package
        fields = ("id", "name", "smd")


class AttributeTypeSerializer(ModelSerializer):
    class Meta:
        model = models.AttributeType
        fields = ("id", "name", "unit", "data_type", "notes")


class AttributeSerializer(ModelSerializer):
    class Meta:
        model = models.Attribute
        fields = ("attribute_type", "value")


class DistributorComponentInfoSerializer(ModelSerializer):
    distributor = DistributorSerializer()

    class Meta:
        model = models.DistributorComponentInfo
        fields = ("id", "ssk", "price", "distributor")


class ComponentSerializer(ModelSerializer):
    attributes = AttributeSerializer(many=True)
    distributors = DistributorComponentInfoSerializer(many=True)
    package = PackageSerializer()

    class Meta:
        model = models.Component
        fields = ("id", "part_id", "manufacturer_pn", "eagle_part", "notes", "in_stock", "manufacturer", "category", "package", "attributes", "distributors")


class CategorySerializer(ModelSerializer):
    components = ComponentSerializer(many=True)
    attribute_types = AttributeTypeSerializer(many=True)

    class Meta:
        model = models.Category
        fields = ("id", "name", "notes", "attribute_types", "components")


class BOMItemSerializer(ModelSerializer):
    class Meta:
        model = models.BOMItem
        fields = ("id", "part", "notes", "component")


class BOMSerializer(ModelSerializer):
    items = BOMItemSerializer(many=True)

    class Meta:
        model = models.BOM
        fields = ("id", "name", "notes", "items")
