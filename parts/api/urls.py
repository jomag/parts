
from django.conf.urls import url, include
from rest_framework import routers

from parts.api import views

router = routers.DefaultRouter()
router.register(r"manufacturers", views.ManufacturerViewSet)
router.register(r"distributors", views.DistributorViewSet)
router.register(r"packages", views.PackageViewSet)
router.register(r"attribute_types", views.AttributeTypeViewSet)
router.register(r"categories", views.CategoryViewSet)
router.register(r"components", views.ComponentViewSet)
router.register(r"boms", views.BOMViewSet)

urlpatterns = [
    url(r"^", include(router.urls))
]
