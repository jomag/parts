
from rest_framework.viewsets import ModelViewSet

from parts.main import models
from parts.api import serializers


class ManufacturerViewSet(ModelViewSet):
    queryset = models.Manufacturer.objects.all().order_by("name")
    serializer_class = serializers.ManufacturerSerializer


class DistributorViewSet(ModelViewSet):
    queryset = models.Distributor.objects.all().order_by("name")
    serializer_class = serializers.DistributorSerializer


class PackageViewSet(ModelViewSet):
    queryset = models.Package.objects.all().order_by("name")
    serializer_class = serializers.PackageSerializer


class AttributeTypeViewSet(ModelViewSet):
    queryset = models.AttributeType.objects.all().order_by("name")
    serializer_class = serializers.AttributeTypeSerializer


class CategoryViewSet(ModelViewSet):
    queryset = models.Category.objects.all().order_by("name")
    serializer_class = serializers.CategorySerializer


class ComponentViewSet(ModelViewSet):
    queryset = models.Component.objects.all().order_by("part_id")
    serializer_class = serializers.ComponentSerializer


class BOMViewSet(ModelViewSet):
    queryset = models.BOM.objects.all().order_by("name")
    serializer_class = serializers.BOMSerializer
