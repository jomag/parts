from django.contrib import admin
from django.db import models
from django import forms

from .models import Manufacturer, Package, Category, CategoryAttributeType
from .models import AttributeType, Component, Attribute, Distributor
from .models import DistributorComponentInfo, BOM, BOMItem


class ComponentAttributeInline(admin.TabularInline):
    model = Attribute
    extra = 1


class DistributorComponentInfoInline(admin.TabularInline):
    model = DistributorComponentInfo
    extra = 1


class ComponentAdmin(admin.ModelAdmin):
    # Define order of fields
    fields = (
        "part_id",
        "category",
        "manufacturer",
        "manufacturer_pn",
        "package",
        "in_stock",
        "eagle_part",
        "notes"
    )
    inlines = [ComponentAttributeInline, DistributorComponentInfoInline]


class CategoryAttributeTypeInline(admin.TabularInline):
    model = CategoryAttributeType
    extra = 1


class CategoryAdmin(admin.ModelAdmin):
    inlines = [CategoryAttributeTypeInline]


class BOMItemInline(admin.TabularInline):
    model = BOMItem
    extra = 10
    fields = ("part", "component", "notes")
    formfield_overrides = {
        models.TextField: {
            "widget": forms.TextInput(attrs={"size": 60})
        }
    }

class BOMAdmin(admin.ModelAdmin):
    inlines = [BOMItemInline]


admin.site.register(Manufacturer)
admin.site.register(Package)
admin.site.register(Category, CategoryAdmin)
admin.site.register(AttributeType)
admin.site.register(Component, ComponentAdmin)
admin.site.register(Distributor)
admin.site.register(DistributorComponentInfo)
admin.site.register(BOM, BOMAdmin)

# admin.site.register(Attribute)
# admin.site.register(BOMItem)
