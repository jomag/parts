
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.views import generic
from django.template.defaulttags import register
from django.views.generic.edit import UpdateView
from django.views.generic.detail import DetailView

from .models import Component, Category, AttributeType, BOM, Manufacturer, Distributor
from .forms import ComponentForm, CategoryForm


def index(request):
    return render(request, "main/start.html")


@register.filter
def attribute_value(attr):
    return attr.attribute_type.format(attr.value)


class ComponentIndexView(generic.ListView):
    model = Component
    template_name = "main/component_index.html"
    context_object_name = "components"

    columns = [
        ('part_id', 'Part ID'),
        ('manufacturer', 'Manufacturer'),
        ('manufacturer_pn', 'Manufacturer P/N'),
        ('category', 'Category'),
        ('package', "Package"),
        ('in_stock', 'In Stock')
    ]

    def get_order(self, default_order="part_id"):
        order = self.request.GET.get("order")

        if order:
            if order[0] == "-":
                reverse = True
                order = order[1:]
            else:
                reverse = False

            if order in [col[0] for col in self.columns]:
                if reverse:
                    order = "-" + order

            return order
        else:
            return default_order

    def get_queryset(self):
        if "pk" in self.kwargs:
            category = get_object_or_404(Category, pk=self.kwargs["pk"])
            q = Component.objects.filter(category=category)
        else:
            q = Component.objects

        order = self.get_order()
        if order:
            q = q.order_by(order)

        return q.all()

    def get_context_data(self, **kwargs):
        ctx = super(ComponentIndexView, self).get_context_data(**kwargs)

        if "pk" in self.kwargs:
            category = get_object_or_404(Category, pk=self.kwargs["pk"])
            ctx["attribute_types"] = [(at, at.name) for at in category.attribute_types.all()]
            ctx["category"] = category
            ctx["title"] = category.name
        else:
            ctx["title"] = "Components"

        ctx["columns"] = self.columns
        ctx["order"] = self.get_order()
        return ctx


class ComponentUpdateView(DetailView):
    model = Component
    template_name = "main/component_detail.html"


class CategoryIndexView(generic.ListView):
    model = Category
    template_name = "main/category_index.html"
    context_object_name = "categories"


class CategoryUpdateView(UpdateView):
    model = Category
    form_class = CategoryForm
    template_name = "main/category_edit.html"


class CategoryDetailView(generic.DetailView):
    model = Category
    template_name = "main/category_detail.html"


class BOMIndexView(generic.ListView):
    model = BOM
    template_name = "main/bom_index.html"
    context_object_name = "boms"


class BOMDetailView(generic.DetailView):
    model = BOM
    template_name = "main/bom.html"
    context_object_name = "bom"

    def get_context_data(self, **kwargs):
        ctx = super(BOMDetailView, self).get_context_data(**kwargs)
        ctx["cols"] = ("parts", "component", "manufacturer", "pn", "in_stock", "cost", "total")
        ctx["rows"], ctx["sum"] = ctx["bom"].process_bom(None)
        return ctx


class BOMExport(generic.DetailView):
    model = BOM
    def get(self, request, *args, **kwargs):
        bom = self.get_object()
        response = HttpResponse(content_type="application/vnd.ms-excel")
        response["Content-Disposition"] = "attachment; filename=\"%s.xlsx\"" % bom.name
        xlsx = bom.export_excel()
        xlsx.save(response)
        return response


class ManufacturerIndexView(generic.ListView):
    model = Manufacturer
    template_name = "main/manufacturer_index.html"
    context_object_name = "manufacturers"


class DistributorIndexView(generic.ListView):
    model = Distributor
    template_name = "main/distributor_index.html"
    context_object_name = "distributors"
