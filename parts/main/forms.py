
from django import forms

from .models import Component, Category


class ComponentForm(forms.ModelForm):
    class Meta:
        model = Component
        fields = ['part_id', 'manufacturer_pn', 'eagle_part', 'notes']


class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ["name", "notes", "attribute_types"]
