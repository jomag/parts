
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):
    # Required for SQLite
    atomic = False

    dependencies = [
        ("main", "0005_allow_null_component_for_bom_item_20170522_1206")
    ]

    operations = [
        migrations.RenameModel("vendor", "manufacturer"),
        migrations.RenameField("component", "vendor", "manufacturer"),
        migrations.RenameField("component", "vendor_sku", "manufacturer_pn"),
        migrations.RenameField("distributorcomponentinfo", "ssk", "sku")
    ]
