
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r"^components$", views.ComponentIndexView.as_view(), name="component_index"),
    url(r"^components/(?P<pk>[0-9]+)/$", views.ComponentUpdateView.as_view(), name="component_edit"),

    url(r"^categories$", views.CategoryIndexView.as_view(), name="category_index"),
    url(r"^categories/(?P<pk>[0-9]+)/$", views.CategoryDetailView.as_view(), name="category_details"),
    url(r"^categories/(?P<pk>[0-9]+)/edit$", views.CategoryUpdateView.as_view(), name="category_edit"),
    url(r"^categories/(?P<pk>[0-9]+)/components$", views.ComponentIndexView.as_view(), name="category_component_index"),

    url(r"^boms$", views.BOMIndexView.as_view(), name="bom_index"),
    url(r"^boms/(?P<pk>[0-9]+)/$", views.BOMDetailView.as_view(), name="bom_details"),
    url(r"^boms/(?P<pk>[0-9]+).xlsx$", views.BOMExport.as_view(), name="bom_export"),

    url(r"^manufacturers$", views.ManufacturerIndexView.as_view(), name="manufacturer_index"),
    url(r"^distributors$", views.DistributorIndexView.as_view(), name="distributor_index")
]
