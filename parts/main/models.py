from django.db import models
from openpyxl import Workbook

UNLIMITED = 1000


class Manufacturer(models.Model):
    name = models.CharField(max_length=UNLIMITED)
    notes = models.TextField(blank=True, null=True)

    class Meta:
        ordering = ("name",)

    def __str__(self):
        return self.name


class Package(models.Model):
    name = models.CharField(max_length=UNLIMITED, unique=True)
    smd = models.BooleanField(default=False)

    class Meta:
        ordering = ("name",)

    def __str__(self):
        return self.name


class AttributeType(models.Model):
    name = models.CharField(max_length=UNLIMITED, unique=True)
    unit = models.CharField(max_length=20, null=True)
    data_type = models.CharField(max_length=20, default="string")
    notes = models.TextField(blank=True, null=True)

    class Meta:
        ordering = ("name",)

    def __str__(self):
        return self.name

    def format(self, value):
        if value is None:
            return ''

        if self.data_type == 'number':
            try:
                value = float(value)
            except ValueError:
                return ''
            except TypeError:
                return ''

            mp = [(1000000, 'M', 1000000),
                  (1000, 'k', 1000),
                  (0.1, '', 1),
                  (0.001, 'm', 0.001),
                  (0.000001, 'µ', 0.000001),
                  (0.000000001, 'n', 0.000000001),
                  (0.000000000001, 'p', 0.000000000001)]

            for n, pfx, d in mp:
                if value >= n:
                    return '%g %s%s' % ((value / d), pfx, self.unit)

            return '%g %s' % (value, self.unit)
        else:
            if self.unit:
                return value + ' ' + self.unit
            else:
                return value


class Category(models.Model):
    name = models.CharField(max_length=UNLIMITED, unique=True)
    notes = models.TextField(blank=True, null=True)
    attribute_types = models.ManyToManyField(AttributeType, through="CategoryAttributeType")

    class Meta:
        ordering = ("name",)
        verbose_name_plural = "categories"

    def __str__(self):
        return self.name


class Component(models.Model):
    part_id = models.CharField(max_length=100, unique=True)
    manufacturer_pn = models.CharField(max_length=100, null=True, blank=True)
    eagle_part = models.CharField(max_length=100, null=True, blank=True)
    notes = models.TextField(blank=True, null=True)
    in_stock = models.IntegerField(null=True, blank=True)

    manufacturer = models.ForeignKey(Manufacturer, related_name="components", null=True, blank=True, on_delete=models.PROTECT)
    category = models.ForeignKey(Category, related_name="components", null=True, blank=True, on_delete=models.PROTECT)
    package = models.ForeignKey(Package, related_name="components", null=True, blank=True, on_delete=models.PROTECT)

    class Meta:
        ordering = ("part_id",)

    def __str__(self):
        if self.part_id and self.manufacturer_pn:
            return "%s: %s" % (self.part_id, self.manufacturer_pn)
        else:
            return self.part_id

    def best_price(self):
        candidates = self.distributors.all()
        if candidates:
            return candidates[0].price
        else:
            return None

    def create_missing_attributes(self):
        if self.category:
            available = [a.attribute_type.id for a in self.attributes.all()]

            for attr_type in self.category.attribute_types.all():
                if attr_type.id not in available:
                    self.attributes.create(attribute_type=attr_type)

    def get_attribute_for(self, attribute_type):
        for attr in self.attributes.all():
            if attr.attribute_type == attribute_type:
                return attr
        return None

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        self.create_missing_attributes()


class Attribute(models.Model):
    value = models.CharField(max_length=UNLIMITED, null=True)
    attribute_type = models.ForeignKey(AttributeType, on_delete=models.PROTECT)
    component = models.ForeignKey(Component, related_name="attributes", on_delete=models.CASCADE)

    def __str__(self):
        return "{}: {} {}".format(self.attribute_type.name, self.value, self.attribute_type.unit)


class CategoryAttributeType(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    attribute_type = models.ForeignKey(AttributeType, on_delete=models.CASCADE)


class Distributor(models.Model):
    name = models.CharField(max_length=UNLIMITED, unique=True)
    notes = models.TextField(blank=True, null=True)
    lookup_url = models.CharField(max_length=UNLIMITED, blank=True, null=True)

    class Meta:
        ordering = ("name",)

    def __str__(self):
        return self.name


class DistributorComponentInfo(models.Model):
    # FIXME: link to distributor and component
    sku = models.CharField(max_length=UNLIMITED, null=True)

    # Allow prices up to 999.999.999,9999999
    price = models.DecimalField(max_digits=16, decimal_places=7, blank=True, null=True)

    distributor = models.ForeignKey(Distributor, related_name="component_infos", on_delete=models.CASCADE)
    component = models.ForeignKey(Component, related_name="distributors", on_delete=models.CASCADE)

    @property
    def distributor_url(self):
        if self.distributor is None:
            return None
        if self.distributor.lookup_url:
            return self.distributor.lookup_url.replace('[SKU]', self.sku)
        return None

    @property
    def price_string(self):
        if self.price is None:
            return ''
        return '%g' % self.price


class BOM(models.Model):
    name = models.CharField(max_length=UNLIMITED, unique=True)
    notes = models.TextField(blank=True, null=True)

    class Meta:
        ordering = ("name",)

    def __str__(self):
        return self.name

    def copy(self, new_name=None):
        if new_name is None:
            # FIXME: continue testing until unique
            new_name = self.name + " (2)"

        c = BOM(name=new_name, notes=self.notes)

        for item in self.items:
            c.items.append(item.copy())

        return c

    def process_bom(self, distributor=None):
        """Returns a BOM with rows grouped by component and distributor info
        for the selected distributor:

        {
          rows: [ { items=[...], component=..., distributor_info=... }, ... ]
          sum: ...
        }
        """
        by_component = {}
        unassigned = []

        for item in self.items.all():
            if item.component:
                if item.component in by_component:
                    by_component[item.component]['items'].append(item)
                else:
                    di = DistributorComponentInfo.objects.filter(component=item.component, distributor=distributor).first()
                    by_component[item.component] = { 'items': [ item ], 'distributor_info': di }
            else:
                unassigned.append(item)

        # Build rows from the unique component list and unassigned
        rows = [ { 'items': row['items'],
                   'component': component,
                   'distributor_info': row['distributor_info'] } for component, row in by_component.items() ]

        for item in unassigned:
            rows.append({ 'items': [ item ], 'component': None, 'distributor_info': None })

        # Sort the parts of each row
        for row in rows:
            row['items'] = sorted(row['items'], key=lambda p: p.part)

        # Sort the rows
        rows = sorted(rows, key=lambda p: p['items'][0].part)

        # Calculate the total sum
        sum = 0
        for row in rows:
            if row['distributor_info']:
                price = row['distributor_info'].price
                if price:
                    sum = sum + len(row['items']) * price

        return rows, sum

    def export_excel(self):
        workbook = Workbook()
        sheet = workbook.active

        header = [
            "Qty",
            "Designator",
            "Part ID",
            "Attributes",
            "Package",
            "Manufacturer",
            "Manufacturer P/N",
            "Cost",
            "Total cost"
        ]
        sheet.append(header)

        rows, _ = self.process_bom()

        for row in rows:
            #import pdb;pdb.set_trace()
            qty = len(row["items"])
            items = ", ".join([item.part for item in row["items"]])
            component = row["component"]

            try:
                part_id = component.part_id
            except AttributeError:
                part_id = ""

            try:
                package_name = component.package.name
            except AttributeError:
                package_name = ""

            try:
                manufacturer_name = component.manufacturer.name
            except AttributeError:
                manufacturer_name = ""

            try:
                manufacturer_pn = component.manufacturer_pn
            except AttributeError:
                manufacturer_pn = ""

            try:
                best_price = component.best_price()
            except AttributeError:
                best_price = None

            if best_price is None:
                total_price = None
            else:
                total_price = best_price * qty

            try:
                attributes = ", ".join(a.attribute_type.format(a.value) for a in component.attributes.all())
            except AttributeError:
                attributes = ""

            sheet.append([
                qty,
                items,
                part_id,
                attributes,
                package_name,
                manufacturer_name,
                manufacturer_pn,
                best_price,
                total_price
            ])

        return workbook


class BOMItem(models.Model):
    # Name of the part in schema ("R3", "C2", ...)
    part = models.CharField(max_length=UNLIMITED)
    notes = models.TextField(blank=True, null=True)
    bom = models.ForeignKey(BOM, related_name="items", on_delete=models.CASCADE)
    component = models.ForeignKey(Component, related_name="component", null=True, blank=True, on_delete=models.PROTECT)

    class Meta:
        ordering = ("part",)

    def __str__(self):
        return self.part

    def copy(self):
        return BOMItem(part=self.part, component=self.component, bom_id=None, notes=self.notes)
