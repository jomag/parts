
from django.contrib.staticfiles.storage import staticfiles_storage
from django.urls import reverse
from jinja2 import Environment


def custom_reverse(name, **kwargs):
    """
    Temporary hack. With Django templates URL's can be reversed like this:

    {{ url "component_detail" pk=3 }}

    In Jinja2 using the raw reverse method the same would be written like this:

    {{ url("component_detail", kwargs={"pk": 3})}}

    This function allows this syntax:

    {{ url("component_detail", pk=3)}}
    """
    return reverse(name, kwargs=kwargs)

def environment(**options):
    env = Environment(**options)
    env.globals.update({
        'static': staticfiles_storage.url,
        'url': custom_reverse,
    })
    return env
