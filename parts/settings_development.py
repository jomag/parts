

from parts.settings_shared import *

DEBUG = True
SECRET_KEY = "devel-key"

ALLOWED_HOSTS = "*"

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'development.sqlite3'),
    }
}
