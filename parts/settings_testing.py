
from parts.settings_shared import *

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3"
    }
}

SECRET_KEY = "test-key"
