
import pytest
from django.core.management import call_command

from parts.main.models import AttributeType, Category, CategoryAttributeType


@pytest.fixture(scope="session")
def django_db_setup(django_db_setup, django_db_blocker):
    with django_db_blocker.unblock():
        call_command("loaddata", "tests/data.json")


@pytest.fixture
def resistor_category():
    cat = Category.objects.create(name="resistors")
    attribute_types = [
        AttributeType.objects.create(name="resistance", unit="ohm", data_type="number"),
        AttributeType.objects.create(name="max power", unit="W", data_type="number"),
        AttributeType.objects.create(name="tolerance", unit="%", data_type="number")
    ]

    for at in attribute_types:
        CategoryAttributeType.objects.create(category=cat, attribute_type=at)

    return cat
