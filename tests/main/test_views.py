
def test_category_index(client, db):
    r = client.get("/categories")
    assert r.status_code == 200


def test_category_detail(client, db):
    r = client.get("/categories/1")
    assert r.status_code == 301
    r = client.get("/categories/1/")
    assert r.status_code == 200


def test_bom_index(client, db):
    r = client.get("/boms")
    assert r.status_code == 200


def test_bom_detail(client, db):
    r = client.get("/boms/1")
    assert r.status_code == 301
    r = client.get("/boms/1/")
    assert r.status_code == 200


def test_component_index(client, db):
    r = client.get("/components")
    assert r.status_code == 200


def test_component_detail(client, db):
    r = client.get("/components/1")
    assert r.status_code == 301
    r = client.get("/components/1/")
    assert r.status_code == 200
