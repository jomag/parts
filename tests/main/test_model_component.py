
import pytest

from parts.main.models import Component


@pytest.mark.django_db
def test_category_attributes_are_created_automatically(resistor_category):
    c = Component.objects.create(category=resistor_category)
    assert len(c.attributes.all()) == 3

    c.create_missing_attributes()
    assert len(c.attributes.all()) == 3
