
from distutils.core import setup


setup(name="parts",
      version="0.0.1",
      description="Parts",
      author="Jonatan Magnusson",
      author_email="jonatan.magnusson@gmail.com",
      url="http://parts.herokuapp.com",
      packages=['parts'])
